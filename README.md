# Minimum Variance Partition #

### What is this repository for? ###

This code uses a quadratic integer programming model (with CPLEX as the solver) to find a partition of a set of positive integers such that (a) no subset sums to more than a specified limit, (b) a minimal number of sets is used in the partition and (c) the variance of the set-by-set sums is minimal.

The problem originates in a [question](https://orinanobworld.blogspot.com/2020/02/reversing-differences.html) on Mathematics Stack Exchange. The results are posted in my answer there. The model is explained in a [blog post](https://orinanobworld.blogspot.com/2020/03/a-minimum-variance-partition-problem.html).

The code was developed using CPLEX 12.10 but will run with at least some earlier versions.

### License ###
The code here is copyrighted by Paul A. Rubin (yours truly) and licensed under a [Creative Commons Attribution 3.0 Unported License](http://creativecommons.org/licenses/by/3.0/deed.en_US). 

