package mvpartition;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloQuadNumExpr;
import ilog.cplex.IloCplex;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Model builds and executes a mixed-integer quadratic programming model
 * for the problem.
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Model implements AutoCloseable {
  /** Item sizes. */
  private static final int[] SIZES =
    new int[] {16, 132, 135, 135, 136, 138, 138, 139, 301, 334, 355, 361};
  /** Maximum weight of any set in the partition. */
  private static final double MAXWEIGHT = 533;
  /** Values for binary variables above this cutoff indicate the binary
   *  variable has value 1 in the solution. */
  private static final double ON = 0.5;

  private final int nSets;             // the cardinality of the partition
  private final double mean;           // mean weight of a subset
  private final IloCplex qp;           // the quadratic program
  private final IloIntVar[][] assign;  // the assignment variables
  private final IloNumVar[] weight;    // set totals

  /**
   * Constructor.
   * @param cardinality the number of sets in the partition
   * @throws IloException if anything goes splat
   */
  public Model(final int cardinality) throws IloException {
    nSets = cardinality;
    int nItems = SIZES.length;
    // Compute the mean weight of a set.
    mean = (double) Arrays.stream(SIZES).sum() / nSets;
    // Set up the model instance.
    qp = new IloCplex();
    qp.setOut(null);
    // Define the assignment variables.
    assign = new IloIntVar[nItems][nSets];
    for (int i = 0; i < nItems; i++) {
      for (int g = 0; g < nSets; g++) {
        assign[i][g] = qp.boolVar("Assign_" + i + "_" + g);
      }
    }
    // Define the set weight variables.
    weight = new IloNumVar[nSets];
    for (int g = 0; g < nSets; g++) {
      weight[g] = qp.numVar(0, MAXWEIGHT, "Total_" + g);
    }
    // Constrain every item to be assigned once.
    for (int i = 0; i < nItems; i++) {
      qp.addEq(qp.sum(assign[i]), 1.0, "Assign_once_" + i);
    }
    // Define the set weight totals.
    for (int g = 0; g < nSets; g++) {
      IloLinearNumExpr expr = qp.linearNumExpr();
      for (int i = 0; i < nItems; i++) {
        expr.addTerm(SIZES[i], assign[i][g]);
      }
      qp.addEq(weight[g], expr, "Calc_wt_" + g);
    }
    // Require at least one item in each subset.
    for (int g = 0; g < nSets; g++) {
      IloLinearNumExpr expr = qp.linearNumExpr();
      for (int i = 0; i < nItems; i++) {
        expr.addTerm(1.0, assign[i][g]);
      }
      qp.addGe(expr, 1.0, "Use_" + g);
    }
    // Mitigate symmetry by enumerating subsets in ascending weight order.
    for (int g = 1; g < nSets; g++) {
      qp.addLe(weight[g - 1], weight[g], "Antisym_" + g);
    }
    // Define the objective function (sum of squared of subset weights).
    IloQuadNumExpr obj = qp.quadNumExpr();
    for (int g = 0; g < nSets; g++) {
      obj.addTerm(1.0, weight[g], weight[g]);
    }
    qp.addMinimize(obj);
  }

  /**
   * Solves the model.
   * @return the final model status
   * @throws IloException if the solver gorks
   */
  public IloCplex.Status solve() throws IloException {
    qp.solve();
    return qp.getStatus();
  }

  /**
   * Gets the variance of a partition.
   * @return the variance
   * @throws IloException if there is no solution
   */
  public double getVariance() throws IloException {
    double var = qp.getObjValue();
    return (var / nSets) - (mean * mean);
  }

  /**
   * Closes the model.
   * @throws IloException if the model is a zombie
   */
  @Override
  public void close() throws IloException {
    qp.end();
  }

  /**
   * Gets the optimal solution.
   * @return an array of object sizes, one row per subset
   * @throws IloException if CPLEX won't cough up the solution
   */
  public ArrayList<ArrayList<Integer>> getSolution() throws IloException {
    ArrayList<ArrayList<Integer>> sol = new ArrayList<>();
    int nItems = SIZES.length;
    // Get the assignment variables.
    double[][] vals = new double[nItems][];
    for (int i = 0; i < nItems; i++) {
      vals[i] = qp.getValues(assign[i]);
    }
    // Convert into an list of lists of object sizes.
    for (int g = 0; g < nSets; g++) {
      ArrayList<Integer> z = new ArrayList<>();
      for (int i = 0; i < nItems; i++) {
        if (vals[i][g] > ON) {
          z.add(SIZES[i]);
        }
      }
      sol.add(z);
    }
    return sol;
  }

  /**
   * Fixes an assignment.
   * This is used for debugging or comparison to outside solutions.
   * @param indices a list (one entry per subset) of lists of item indices
   * @throws IloException if CPLEX will not let the assignments be made.
   */
  public void fix(final List<List<Integer>> indices)
              throws IloException {
    for (int g = 0; g < nSets; g++) {
      for (int i : indices.get(g)) {
        assign[i][g].setLB(1.0);
      }
    }
  }

  /**
   * Exports the model.
   * This is intended for debugging.
   * @param target the destination for the model
   * @throws IloException if CPLEX gets snippy
   */
  public void exportModel(final String target) throws IloException {
    qp.exportModel(target);
  }

  /**
   * Gets the number of items in the parent set.
   * @return the item count
   */
  public static int itemCount() {
    return SIZES.length;
  }
}
