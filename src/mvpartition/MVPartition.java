package mvpartition;

import ilog.concert.IloException;
import ilog.cplex.IloCplex;
import java.util.ArrayList;

/**
 * MVPartition solves a problem posed on Mathematics Stack Exchange
 * (https://math.stackexchange.com/questions/3594343/combinatorial-optimization-
 * find-partitions/3597944)
 * by iteratively trying all possible cardinalities of the partition.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class MVPartition {

  /**
   * Dummy constructor.
   */
  private MVPartition() { }

  /**
   * Finds minimum variance partitions for each feasible number of sets in
   * the partition and displays results.
   * @param args the command line arguments (none used)
   */
  public static void main(final String[] args) {
    double minvar = Double.MAX_VALUE;
    ArrayList<ArrayList<Integer>> incumbent = null;
    for (int n = 1; n <= Model.itemCount(); n++) {
      System.out.println("\n::: Trying " + n + " subsets :::\n");
      try (Model model = new Model(n)) {
        IloCplex.Status status = model.solve();
        if (status == IloCplex.Status.Infeasible) {
          System.out.println("Result: infeasible.");
        } else if (status == IloCplex.Status.Optimal) {
          double var = model.getVariance();
          System.out.println("Result: variance of " + var + ".");
          if (var < minvar) {
            minvar = var;
            incumbent = model.getSolution();
          }
        } else {
          System.out.println("Unhelpful final status: " + status + ".");
        }
      } catch (IloException ex) {
        System.err.println("CPLEX blew up:\n" + ex.getMessage());
        break;
      }
    }
    if (incumbent != null) {
      System.out.println("\nOptimal partition:");
      for (ArrayList<Integer> x : incumbent) {
        System.out.println(x);
      }
    }
  }

}
